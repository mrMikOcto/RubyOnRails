class Profile < ActiveRecord::Base
  belongs_to :user

  validate :at_least_one_is_not_empty
  validates :gender, inclusion: { in: %w(male female) }
  validate :first_name_not_sue

  def at_least_one_is_not_empty
  	if first_name.nil? and last_name.nil?
  		errors.add(:first_name, "is nill")
  		errors.add(:last_name, "is nill")
  	end  	
  end

  def first_name_not_sue
  	if first_name == "Sue" and gender == "male"
  		errors.add(:first_name, "can't be Sue")
  	end
  end

  def self.get_all_profiles2(min, max)
  	where("birth_year BETWEEN :min_age AND :max_age", min_age: min, max_age: max).order(:birth_year)
  end

  scope :get_all_profiles, -> (min, max){ where("birth_year BETWEEN :min_age AND :max_age", min_age: "#{min}", max_age: "#{max}").order(:birth_year) }
end
