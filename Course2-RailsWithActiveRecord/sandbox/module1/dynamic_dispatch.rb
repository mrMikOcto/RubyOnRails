require_relative 'store'
class ReportingSystem
	def initialize
		@store = Store.new
	end
	def get_piano_desc
		@store.get_piano_desc
	end
	def get_piano_price
		@store.get_piano_price
	end
end

class BetterReportingSystem
	def initialize
		@store = Store.new
	end
	def method_missing(name, *args)
		super unless @store.respond_to?(name)
		@store.send(name)
	end
end


puts "ReportingSystem:"
rs = ReportingSystem.new
puts "\t#{rs.get_piano_desc} costs #{rs.get_piano_price.to_s.ljust(6, '0')}"

puts "BetterReportingSystem:"
brs = BetterReportingSystem.new
puts "\t#{brs.get_piano_desc} costs #{brs.get_piano_price.to_s.ljust(6, '0')}"