# Classes
class Person
	def initialize (name, age) # CONSTRUCTOR
		@name = name
		@age = age
	end
	def get_info
		@additional_info = "Interesting"
		"Name: #{@name}, age: #{@age}"
	end
	def name #getter
		@name
	end
	def name= (new_name) # setter
		@name = new_name
	end
end

person1 = Person.new("Joe", 14)
puts "Classes:"
p person1.instance_variables
puts person1.get_info
p person1.instance_variables
puts person1.name
person1.name = "Mike"
puts person1.name
begin
	puts person1.age
rescue Exception => e
	puts e.message
end

# attr_* instead manually defining getter/setter
# 	attr_accessor:	getter and setter
#   attr_reader:	getter only
# 	attr_writer: 	setter only
class PersonN
	attr_accessor :name, :age
end
puts "\nQuickly attributes getter/setter:"
personN1 = PersonN.new
p personN1.name
personN1.name = "Mike"
personN1.age = 15
puts personN1.name
puts personN1.age
personN1.age = "fifteen"
puts personN1.age

# self
class PersonSelf
	attr_reader :age
	attr_accessor :name

	def initialize (name, age)
		@name = name
		self.age = age # call the age= method
	end
	def age= (new_age)
		@age ||= 5 # default
		@age = new_age unless new_age > 120
	end
end
puts "\nPerson self:"
person_self = PersonSelf.new("Kai", 17)
puts "My age is #{person_self.age}"
person_self.age = 130
puts "New person age #{person_self.age}"

puts "\nDefault value: "
pers_self = PersonSelf.new("Karen", 130)
puts pers_self.age
pers_self.age = 10
puts pers_self.age
pers_self.age = 200
puts pers_self.age

# Class Methods and Variables
puts "\nClass Methods and Variables (like a \"static\"):"
class MathFunctions
	def self.double(var) # 1. Using self
		times_called
		var *2
	end
	class << self # 2. Using << self
		def times_called
			@@times_called ||=0
			@@times_called += 1
		end
	end
end
def MathFunctions.triple(var) # 3. Outside of class
	times_called
	var * 3
end

# No instance created!
puts MathFunctions.double 5
puts MathFunctions.triple(3)
puts MathFunctions.times_called

# Class inheritance
puts "\nClass inheritance:"
class Dog
	def to_s
		"Dog"
	end
	def bark
		"barks loudly"
	end
end
class SmallDog < Dog
	def bark # Override
		"barks quietly"
	end
end

dog = Dog.new
small_dog = SmallDog.new
puts "#{dog}1 #{dog.bark}"
puts "#{small_dog}2 #{small_dog.bark}"


# MODULE AND NAMESPACE
module Sports
	class Match
		attr_accessor :score
	end
end

module Patterns
	class Match
		attr_accessor :complete
	end
end

match1 = Sports::Match.new
match1.score = 45

match2 = Patterns::Match.new
match2.complete = true

# Module as Mixin
module SayMyName
	attr_accessor :name
	def print_name
		puts "Name: #{@name}"
	end
end

class Pers17
	include SayMyName
end
class Company
	include SayMyName
end
puts "\nModule as Mixin:"
person = Pers17.new
person.name = "Joe"
person.print_name
company = Company.new
company.name = "Google & Microsoft LLC"
company.print_name

# Scope
v1 = "Outside"

class MyClass
	def my_method
		# p v1
		v1 = "inside"
		p v1
		p local_variables
	end
end

puts "\nScope:"
p v1
obj = MyClass.new
obj.my_method
p local_variables
p self

# Encapsulation
puts
puts "Encapsulation:"
class Car
	def initialize(speed, comfort)
		@rating = speed * comfort
	end

	# Can't SET rating from outside
	def rating
		@rating
	end
end

puts "Rating: #{Car.new(4, 5).rating}"

puts "\nAccess Control"
class MyAlgorithm
	private
	def test1
		"Private"
	end
	protected
	def test2
		"Protected"
	end
	public
	def public_again
		"Public"
	end
end

class Another
	def test1
		"Private, as decllared later on"
	end
	private :test1
end