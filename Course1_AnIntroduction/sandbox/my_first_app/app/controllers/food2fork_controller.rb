class Food2forkController < ApplicationController
  def index
  	@search_term = params[:looking_for] || 'chocolate'
  	@foods = Food2fork.for(@search_term)
  end
end
