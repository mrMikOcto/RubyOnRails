class Food2fork
	include HTTParty
	base_uri 'http://food2fork.com/api'
	default_params key: 'af394d25523e75d83882f333e34be417'
	#ENV['FOOD2FORK_KEY']
	format :json

	def self.for term
		get(base_uri + "/search", query: { q: term})["recipes"]
	end
end