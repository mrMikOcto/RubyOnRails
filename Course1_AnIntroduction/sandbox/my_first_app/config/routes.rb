Rails.application.routes.draw do
  get 'food2fork/index'

  get 'courses/index'

  # get 'greeter/hello'
  get 'greeter/hello' => "greeter#hello"
  get 'greeter/goodbye'


  root 'food2fork#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
