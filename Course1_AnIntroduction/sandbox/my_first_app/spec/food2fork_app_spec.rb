# Capybara.default_driver = :selenium
# headless
Capybara.default_driver = :poltergeist

Capybara.app_host = "http://search-food2fork-choco.herokuapp.com/"

describe "Food2Fork App" do

	describe "visit root" do
		before { visit '/' }

		it "displays 'Their Pod' (default)" do
			expect(page).to have_content 'Their Pod'
		end

		it "displays table element that has a row with 5 columns" do
			expect(page).to have_selector(:xpath, "//table//tr[count(td)=5]")
		end

		it "column 1 should have the thumbnail inside img tag" do
			expect(page).to have_selector(:xpath, "//table//tr/td[1]//img")
		end

		it "displays 'DASH Diet Mexican Bake' when looking_for=diet" do
			visit "?looking_for=diet"
			expect(page).to have_content 'DASH Diet Mexican Bake'
		end
	end
	
end