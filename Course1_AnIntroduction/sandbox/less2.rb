#String
puts "String: "
my_name = " tim"
puts my_name.lstrip.capitalize
p my_name
my_name.lstrip! # (destructive) removes the leading space
my_name[0] = 'F'
puts my_name

cur_weather = %Q{It's a hot day outside
				 Grab your umbrellas...}

cur_weather.lines do |line|
	line.sub! 'hot', 'rainy' # substitute 'hot' with \rainy'
	puts "#{line.strip}"
end

# Arrays
puts
puts "Arrays:"
het_arr = [1, "two", :three] # heterogenius types
puts het_arr[1]
arr_words = %w{ what a great day today! }
puts arr_words[-2]

puts "#{arr_words.first} - #{arr_words.last}"
p arr_words[-3, 2]

p arr_words[2..4]

puts arr_words.join(',')

# You want a stack (LIFO)? Sure
stack = []
stack << "one"
stack.push("two")
puts stack.pop

# You need a queue (FIFO)? We have those too...
queue = []
queue.push "one"
queue.push "two"
puts queue.shift

a =[5,3,4,2].sort!.reverse!
p a
p a.sample(2)

a[6] = 33
p a

# Array processing
puts
puts "Array processing: "

a = [1,3,4,7,8,10]
a.each { |num| print num }
puts

puts "num > 4"
new_arr = a.select { |num| num > 4 }
p new_arr

puts "num < 10"
new_arr = a.select { |num| num < 10 }
			.reject { |num| num.even? }
p new_arr

# Multiply each element by 3 producing new array
new_arr = a.map { |x| x * 3 }
p new_arr

# RANGES
puts "\nRanges:"
some_range = 1..3
puts some_range.max
puts some_range.include? 2

puts (1...10) === 5.3
puts ('a'...'r') === "r"

p ('k'..'z').to_a.sample(2)

age = 55
case age
	when 0..12 then puts "Still a baby"
	when 13..99 then puts "Teenager at heart!"
	else puts "You are getting older..."
end

# Hashes	
puts "\nHashes"
editor_props = {"font" => "Arial", "size" => 12, "color" => "red"}

# THE ABOVE IS NOT A BLOCK - IT'S A HASH
puts editor_props.length
puts editor_props["font"]

editor_props["background"] = "Blue"
editor_props.each_pair do |key, value|
	puts "Key: #{key} value: #{value}"
end

puts
word_frequency = Hash.new(0)
sentence = "Chicka chicka boom boom fucks"
sentence.split.each do |word|
	word_frequency[word.downcase] += 1
end

p word_frequency
puts

family_tree_19 = {oldest: "Jim", older: "Joe", younger: "Jack"}
family_tree_19[:youngest] = "Jeremy"
p family_tree_19

# Named parameter "like" behavior...
def adjust_colors (props = {foreground: "red", background: "white"})
	puts "Foreground: #{props[:foreground]}" if props[:foreground]
	puts "background: #{props[:background]}" if props[:background]
end
puts
adjust_colors
adjust_colors ({ :foreground => "green" })
adjust_colors background: "yella"
adjust_colors :background => "magenta"
