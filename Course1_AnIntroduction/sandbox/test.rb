a = 5 # declare a variable
b = 0
times_2 = 2

if a == 3
	puts "a s 3"
elsif a == 5
	puts "a is 5"
else
	puts "a is not 3 or 5"
end

puts "One liner" if a == 5 and b == 0
times_2 *= 2 while times_2 < 100
puts times_2

if /sera/ === "coursera"
	puts "Triple equals"
end

if "cpursera" === "cpursera"
	puts "also works"
end

if Integer === 21
	puts "32 is an Integer"
end

# case

age = 21
case # 1ST FLAVOR
	when age >= 21
		puts "You can buy a drink"
	when 1 == 0
		puts "Written by a drunk programmer"
	else
		puts "Default condition"
end

name = 'Fisher'
case name
	when /fish/i then puts "Something is fishy here"
	when 'Smith' then puts "Your name is Smith"
end

# For Loop
for i in 0..2
	puts i
end

# FUNCTIONS
def can_divide_by?(number)
	return false if number.zero?
	true
end

puts can_divide_by? 3
puts can_divide_by? 0


# default arguments
def factorial (n)
	n ==0? 1 : n * factorial(n - 1)
end

def factorial_with_default (n = 5)
	n ==0? 1 : n * factorial_with_default(n - 1)
end

puts factorial 5
puts factorial_with_default
puts factorial_with_default(3)
puts factorial_with_default 4

# SPLAT
def max(one_param, *numbers, another)
	# Variable length parameters passed in
	# become array
	puts "max: "
	numbers.max
end
puts "\nSPLAT, MULTIPLE PARAMETERS *param:"
puts max("something", 7, 32, -4, "more")

# BLOCKS
puts "\nBLOCKS:"
1.times { puts "Hello world!" }

2.times do |index|
	if index > 0
		puts index
	end
end

2.times { |index| puts index if index > 0 }

# implicit 
def two_times_implicit
	return "No block" unless block_given?
	yield
	yield
end
puts "\nIMPLICIT:"
puts two_times_implicit { print "Hello " }
puts two_times_implicit

# explicit
def two_times_explicit (&i_am_a_block)
	return "No block" if i_am_a_block.nil?
	i_am_a_block.call
	i_am_a_block.call
end

puts "\nEXPLICIT:"
puts two_times_explicit
puts two_times_explicit { puts "Hello" }

# FILES
puts "\nFILES:"
File.foreach( 'test.txt' ) do |line|
	puts line
	p line
	p line.chomp # chops off newline character
	p line.split # array of words in line
end

# Handling Exceptions
puts "\nException:"
begin 
	File.foreach( 'do_not_exist.txt' )  do |line|
		puts line.chomp
	end
rescue Exception => e
	puts e.message
	puts "Let's pretend this didn't happen..."
end

# File existing
puts "\nFile exists: "
if File.exist? 'test.txt'
	File.foreach( 'test.txt' ) do |line|
		puts line.chomp
	end
end

# Write to File
print "Writing to File..."
File.open("test1.txt", "w") do |file|
	file.puts "One line"
	file.puts "Another"
end
puts "Ok"

# Enviroment Variables
puts "\nEnviroment Variables:"
print "Editor: "
puts ENV["EDITOR"]