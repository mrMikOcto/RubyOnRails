require 'httparty'
require 'pp'

class HashVault
	include HTTParty
	base_uri 'https://api.coursera.org/api/courses.v1'
	default_params fields: "name", q: "search"
	format :json

	def self.for term		
		get("", query: { query: term})["elements"]
	end
end

pp HashVault.for "python"
