If you need to deploy to Heroku you must initialize git repository in your
project!

	(myapp) $ git init
	(myapp) $ git add . && git commit -m "init commit"
	(myapp) $ heroku login
	(myapp) $ heroku create myapp
		Creating ⬢ ireviewer-books... done
		https://ireviewer-books.herokuapp.com/ | https://git.heroku.com/ireviewer-books.git
	(myapp) $ git remote -v
		heroku	https://git.heroku.com/ireviewer-books.git (fetch)
		heroku	https://git.heroku.com/ireviewer-books.git (push)
	(myapp) $ git push heroku master
		Counting objects: 109, done.
		Delta compression using up to 8 threads.
		Compressing objects: 100% (98/98), done.
		Writing objects: 100% (109/109), 25.26 KiB | 2.30 MiB/s, done.
		Total 109 (delta 8), reused 0 (delta 0)
		remote: Compressing source files... done.
		remote: Building source:
		remote: 
		remote: -----> Ruby app detected
		remote: -----> Compiling Ruby/Rails
		remote: -----> Using Ruby version: ruby-2.4.4
		remote: -----> Installing dependencies using bundler 1.15.2
		......
		remote: Verifying deploy... done.
		To https://git.heroku.com/ireviewer-books.git
		 * [new branch]      master -> master
	(myapp) $


